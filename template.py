import socket
from flask import Flask, request, render_template
from flask_socketio import SocketIO, emit

appPort = 3000

app = Flask(__name__)
app.config['SECRET_KEY'] = 'NEW_SECRET_KEY'
socketio = SocketIO(app)

@app.route("/<channel>",methods=['GET'])
def rlogs_get(channel):
    return render_template('template.html',channel=channel)

@socketio.on('test',namespace='')
def testData(data):
    print ("Test data: " + str(data))
    socketio.emit('test',"Server (template.py): You sent the following data: " + str(data))

if __name__ == '__main__':
    socketio.run(app,'0.0.0.0',appPort)
